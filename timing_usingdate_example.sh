#!/usr/bin/env bash
cd /global/cscratch1/sd/minxu/test_postprocess_perf
date > time_old.log
./alm2ilamb_wkflow/clm_singlevar_ts.csh --caseid F_acmev03_enso_ne30_knl_cesmmach_co2cyc_pmpet_25yr_climpac -T 19,20 -y 1996-2016 \
     -i /global/cscratch1/sd/minxu/test_postprocess_perf/mycase -o /global/cscratch1/sd/minxu/test_postprocess_perf/out_old -e enso -m e3sm --numcc 6 --cmip --ilamb 2>&1
date >> time_old.log
