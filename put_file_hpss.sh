#!/bin/bash

set -x 
#casename=/global/cscratch1/sd/minxu/acme_scratch/archive/F_acme_enso_camse_clm45bgc_ne30_25yr/

#casename=/scratch1/scratchdirs/minxu/acme_scratch/archive/F_acme_enso_camse_clm45bgc_ne30_25yr_edison
#casename=/global/cscratch1/sd/minxu/acme_scratch/archive/F_acme_enso_camse_clm45bgc_ne30
#casename=/global/cscratch1/sd/minxu/archive/F_acmev03_enso_ne30_knl_cesmmach
#casename=/global/cscratch1/sd/minxu/F_acmev03_enso_ne30_knl_cesmmach_co2cyc_pmpet_25yr_ens2
#casename=/global/cscratch1/sd/minxu/acme_scratch/cori-knl/20180914_BGCEXP_BCRC_CNPRDCTC_2000SPINUP_AMIP_DECK.ne30_oECv3.cori
#casename=/global/cscratch1/sd/minxu/F_acmev03_enso_ne30_knl_cesmmach_co2cyc_pmpet_25yr_climatl_1982strt
casename=/global/cscratch1/sd/minxu/F_acmev03_enso_ne30_knl_cesmmach_co2cyc_pmpet_25yr_climpac_1982strt

#component=(atm  cpl  dart  glc  ice  lnd  ocn  rest  rof  wav)
component=(run bld)
#component=(atm  ice  ocn  rof  wav)
#component=(cpl dart glc)
#component=(lnd rest)

#hpssdir=/home/m/minxu/BGC/enso/2nd_25yr_edison/
#hpssdir=/home/m/minxu/BGC/enso/spin-up/
#hpssdir=/home/m/minxu/BGC/enso/spin-up_co2cycle_cori_knl
#hpssdir=/home/m/minxu/BGC/enso/ARCHIEVE/F_acmev03_enso_ne30_knl_cesmmach_co2cyc_pmpet_25yr_ens2
#hpssdir=/home/m/minxu/BGC/enso/E3SMv1-enso
#hpssdir=/home/m/minxu/BGC/enso/F_acmev03_enso_ne30_knl_cesmmach_co2cyc_pmpet_25yr_climatl_1982_strt
hpssdir=/home/m/minxu/BGC/enso/F_acmev03_enso_ne30_knl_cesmmach_co2cyc_pmpet_25yr_climpac_1982_strt

lev=1
crtsz=800 #unit is GB


for cmp in ${component[@]}; do
   #echo $cmp
   #hsi "prompt off; lcd $casename; cd $hpssdir; mput -R $cmp"
   for c in $cmp; do

       if [[ -d "$casename/$c" ]]; then

           #mxu less than the levels, the directory should be created, and files should be tared
           for ((l=1; l<lev; l++)); do
               dirlst=`find $casename/$c -maxdepth $l -mindepth $l -type d -exec echo '{}' \;`
               for d in ${dirlst[@]}; do
                   bas=`basename $d`
                   dir=`dirname  $d`
                   rph=${d#$casename}
                   hps=$hpssdir/$rph
                   #echo $l -- $bas, $hps, $rph
                   hsi "mkdir -p $hps"
               done 
               fillst=`find $casename/$c -maxdepth $l -mindepth $l -type f -exec echo '{}' \;`

               if [[ ! -z "${fillst// }" ]]; then
                  echo $fillst, $l, 'aaa'
               fi
           done

           #begin list the tar files
           dirlst=`find $casename/$c -maxdepth $((lev-1)) -mindepth $((lev-1)) -type d -exec echo '{}' \;`
           for d in ${dirlst[@]}; do
               bas=`basename $d`
               dir=`dirname  $d`
               rph=${d#$casename}
               hps=$hpssdir/$rph
               hsi "mkdir -p $hps"
               echo $bas, $rph
               /bin/rm -f $bas.txt

               fillst=`find $d -maxdepth 10 -mindepth 1 -type f -name "*.nc" -exec echo '{}' \;`

               echo "finish the finding"
               if [[ ! -z "${fillst// }" ]]; then
                   cumsz=0
                   filvr=0
                   for s in ${fillst[@]}; do
                       sz=`stat --printf="%s" $s`
                       filsz=`echo $sz/1000000000. |bc -l`
                       cumsz=`echo $cumsz+$filsz   |bc -l`

                       tarnm=$hps/$bas.tar

                       echo $s >> $bas.txt
                       if (( $(echo "$cumsz > $crtsz" |bc -l) )); then
                          echo $s,$sz, $cumsz, $crtsz
                          tarnm=$hps/$bas$filvr.tar

                          #check if exist in HPSS
                          htar -tf $tarnm > /dev/null
                          if [ $? -eq 0 ]; then
                              echo "find tar in the HPSS, checking its content"
                              htar -tf $tarnm |sed 's/\s\s*/ /g' |cut -d' ' -f7 |sed '/HTAR_CF_CHK/d' |sed '/^\s*$/d' > hpss_$bas.txt
                              diff hpss_$bas.txt $bas.txt > /dev/null
                              comp=$?
                              if [ $comp -eq 0 ]; then
                                 echo "they are same, skip htar"
                              else
                                 echo "they are NOT same, redo htar"
                                 htar -cvf $tarnm -L $bas.txt 
                              fi
                          else
                              echo "create a NEW tar"
                              htar -cvf $tarnm -L $bas.txt 
                          fi

                          cumsz=0
                          filvr=$((filvr+1))
                          /bin/rm -f $bas.txt
                          /bin/rm -f hpss_$bas.txt
                       fi
                   done


                   # last tar or the single tar
                   if [ -f $bas.txt ]; then
                      echo "loop exit with a file list" $cumsz

                      #check if exist in HPSS
                      htar -tf $tarnm > /dev/null
                      if [ $? -eq 0 ]; then
                          echo "find tar in the HPSS, checking its content"
                          htar -tf $tarnm |sed 's/\s\s*/ /g' |cut -d' ' -f7 |sed '/HTAR_CF_CHK/d' |sed '/^\s*$/d' > hpss_$bas.txt
                          diff hpss_$bas.txt $bas.txt > /dev/null
                          comp=$?
                          if [ $comp -eq 0 ]; then
                              echo "they are same, skip htar"
                          else
                              echo "they are NOT same, redo htar"
                              htar -cvf $tarnm -L $bas.txt 
                          fi
                      else
                          echo "create a NEW tar"
                          htar -cvf $tarnm -L $bas.txt 
                      fi
                      /bin/rm -f $bas.txt
                      /bin/rm -f hpss_$bas.txt
                   fi
               fi

           done 
          
           # from this level, find 

           #-fillst=`find $casename/$f -maxdepth 10 -mindepth $lev -type f -exec echo '{}' \;`
           #-if [[ ! -z "${fillst// }" ]]; then
           #-    cumsz=0
           #-    for s in ${fillst[@]}; do
           #-        sz=`stat --printf="%s" $s`
           #-        filsz=`echo $sz/1000000000. |bc -l`
           #-        cumsz=`echo $cumsz+$sz |bc -l`
           #-        dirnm=`dirname $s`
           #-        tarnm=`basename $dirnm`

           #-        echo $tarnm $s >> aaa.txt
           #-        #-if (( $(echo "$cumsz > $crtsz" |bc -l) )); then
           #-        #-   echo $s,$sz, $cumsz
           #-        #-   tarnm=

           #-        #-   #har -cvf test.tar -L aaa.txt 
           #-        #-   cumsz=0
           #-        #-   /bin/rm -f aaa.txt
           #-        #-fi
           #-    done
           #-fi
       fi
   done
done
